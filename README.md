# OpenStack podman Lab

Create an openstack lab on podman containers

# Generates Docker files 
We will be using Kolla to generates de base container files. 

```bash
mkdir kolla-build
cd kolla-build
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install kolla

# Example: generating ubuntu imgages for openstack wallaby release
kolla-build -b ubuntu --registry 192.168.1.3:4000 --template-only --tag wallaby \
--docker-healthchecks --openstack-release wallaby
```

# Create VMs

We are using fixed mac addresses for the pxe interface because we've made dhcp reservation.

```bash
virt-install --name infra1 --cpuset=auto --memory 12288 --vcpus 4 --os-variant fedora33 \
--disk pool=default,size=120,bus=scsi \
--pxe  --boot hd,network \
--network=bridge:vmbr0,mac=52:54:00:7e:da:81,virtualport_type=openvswitch,model=vmxnet3  \
--network=bridge:vmbr0,virtualport_type=openvswitch,model=vmxnet3  \
--noautoconsole \
--cpu host-passthrough --hvm --vnc &

# infra with ssd lvm disk
virt-install --name infra2 --cpuset=auto --memory 12288 --vcpus 4 --os-variant fedora33 \
--disk pool=default,size=120,bus=scsi \
--disk pool=nvme,size=750,bus=scsi \
--pxe  --boot hd,network \
--network=bridge:vmbr0,mac=52:54:00:7e:da:82,virtualport_type=openvswitch,model=vmxnet3  \
--network=bridge:vmbr0,virtualport_type=openvswitch,model=vmxnet3 \
--noautoconsole \
--cpu host-passthrough --hvm --vnc &

virt-install --name infra3 --cpuset=auto --memory 12288 --vcpus 4 --os-variant fedora33 \
--disk pool=default,size=120,bus=scsi \
--disk pool=sshd,size=1500,bus=scsi \
--pxe  --boot hd,network \
--network=bridge:vmbr0,mac=52:54:00:7e:da:83,virtualport_type=openvswitch,model=vmxnet3  \
--network=bridge:vmbr0,virtualport_type=openvswitch,model=vmxnet3 \
--noautoconsole \
--cpu host-passthrough --hvm --vnc &

for i in $(echo {1..2})
do
virt-install --name compute$i --cpuset=auto --memory 28672 --vcpus 10 --os-variant fedora33 \
--disk pool=nvme,size=320,bus=scsi \
--pxe  --boot hd,network \
--network=bridge:vmbr0,mac=52:54:00:7b:da:8$i,virtualport_type=openvswitch,model=vmxnet3  \
--network=bridge:vmbr0,virtualport_type=openvswitch,model=vmxnet3 \
--noautoconsole \
--cpu host-passthrough --hvm --vnc &
sleep 180
done
```

# Install zfs on fedora

```bash
https://openzfs.github.io/openzfs-docs/Getting%20Started/Fedora.html
```
